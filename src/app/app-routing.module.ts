import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectsListComponent } from './projects-list/projects-list.component';
import { LoginComponent } from './login/login.component';
import { TemplatesListComponent } from './templates-list/templates-list.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { TemplateComponent } from './template/template.component';
import { ProjectComponent } from './project/project.component';


const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'statistics', component: StatisticsComponent},
  {path: 'templates-list', component: TemplatesListComponent},
  {path: 'template', component: TemplateComponent},
  {path: 'project', component: ProjectComponent},
  {path: 'projects-list', component: ProjectsListComponent},
  {path: '', redirectTo:'/projects-list', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
