import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProjectsListComponent } from './projects-list/projects-list.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LoginComponent } from './login/login.component';
import { TemplatesListComponent } from './templates-list/templates-list.component';
import { TemplateComponent } from './template/template.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { ProjectComponent } from './project/project.component';
@NgModule({
  declarations: [
    AppComponent,
    ProjectsListComponent,
    NavbarComponent,
    LoginComponent,
    TemplatesListComponent,
    TemplateComponent,
    StatisticsComponent,
    ProjectComponent
  ],
  entryComponents: [
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
