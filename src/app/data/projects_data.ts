export const projects = [
    {
        "siuntimo_pradzia" : "2020-05-04",
        "kliento_el_pastas" : "klientas1@gmail.com",
        "sablonas" : "Kvietimas",
        "projektas" : "Projektas1"
    },
    {
        "siuntimo_pradzia" : "2020-05-09",
        "kliento_el_pastas" : "klientas2@gmail.com",
        "sablonas" : "Priminimas",
        "projektas" : "Projektas2"
    },
    {
        "siuntimo_pradzia" : "2020-05-10",
        "kliento_el_pastas" : "klientas3@gmail.com",
        "sablonas" : "Kvietimas",
        "projektas" : "Projektas3"
    },
    {
        "siuntimo_pradzia" : "2020-05-10",
        "kliento_el_pastas" : "klientas3@gmail.com",
        "sablonas" : "Kvietimas",
        "projektas" : "Projektas3"
    }
]

export const templates = [
    {
        "name" : "Kvietimas"
    },
    {
        "name" : "Priminimas"
    },
    {
        "name" : "Pranešimas"
    },
]