import { Component, OnInit } from '@angular/core';
import { projects } from '../data/projects_data';

@Component({
  selector: 'app-projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: ['./projects-list.component.css']
})
export class ProjectsListComponent implements OnInit {

  projects: Array<any> = projects;

  constructor() { }

  ngOnInit(): void {
  }

}
