import { Component, OnInit } from '@angular/core';

import { templates } from '../data/projects_data';

@Component({
  selector: 'app-templates-list',
  templateUrl: './templates-list.component.html',
  styleUrls: ['./templates-list.component.css']
})
export class TemplatesListComponent implements OnInit {

  templates: Array<any> = templates;
  constructor() { }

  ngOnInit(): void {
  }

}
